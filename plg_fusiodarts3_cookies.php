<?php

/*
  Plugin Name:    Fusiodarts3_Cookies
  Edition:        Basic Edition
  Plugin URI:     http://www.fusiodarts.com
  Description:    Aviso para cumplir la ley de cookies de los clientes de Fusió d'Arts
  Version:        3.0.0
  Author:         Angel Albiach
  Author URI:     http://www.fusiodarts.com

  Copyright (C) 2014, Fusió d'Arts
  All rights reserved.

  Este plugin utiliza GNU General Public License, para más información visite la página http://es.wikipedia.org/wiki/GNU_General_Public_License .
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

/**
 * @package        Joomla
 * @subpackage    System
 */
class plgSystemPlg_Fusiodarts3_Cookies extends JPlugin
{

    /**
     * Constructor
     *
     * For php4 compatability we must not use the __constructor as a constructor for plugins
     * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
     * This causes problems with cross-referencing necessary for the observer design pattern.
     *
     * @access    protected
     * @param    object $subject The object to observe
     * @param     array  $config  An array that holds the plugin configuration
     * @since    1.0
     */
    function plgSystemPlg_Fusiodarts3_Cookies(& $subject, $config) {
        parent::__construct($subject, $config);

        if (JFactory::getApplication()->isAdmin()) return;

        $this->loadLanguage();

        $document = JFactory::getDocument();
        $document->addStyleSheet(JURI::base(true) . '/media/plg_fusiodarts3_cookies/media/plg_fusiodarts3_cookies.css');
        $document->addScript(JURI::base(true) . '/media/plg_fusiodarts3_cookies/media/plg_fusiodarts3_cookies.js');
        $document->addScript(JURI::base(true) . '/media/plg_fusiodarts3_cookies/media/jquery-cookie.js');
    }

    /**
     * Start the output
     *
     */
    function onAfterRender() {
        if (JFactory::getApplication()->isAdmin()) return;

        $checkCookie = JFactory::getApplication()->input->cookie->get('accept_cookie');
        if ($checkCookie != "Si") {

            //Load language
            $language = JFactory::getLanguage();
            $language->load('plg_system_plg_fusiodarts3_cookies', JPATH_ADMINISTRATOR);

            global $mainframe, $database;

            //get Params
            $message = $this->params->get('mensaje', '');
            $buttomText = $this->params->get('buttomText', '');
            $URLLegal = $this->params->get('urlLegal', '');
            $urlLegalText = $this->params->get('urlLegalText', '');
            $posicion = $this->params->get('posicion', '');
            $button_style = $this->params->get('button_style', '');
            $jquery_charge = $this->params->get('jquery_charge', '');
            $agreeCookies = $this->params->get('agreeCookies', '');
            $agreeAnalitycs = $this->params->get('agreeAnalitycs', '');

            $document = JFactory::getDocument();
            $doctype = $document->getType();
            $app = JFactory::getApplication();

            $ICON_FOLDER = JURI::base(true) . '/media/plg_fusiodarts3_cookies/media/images/';

            if ($app->getClientId() === 0) {

                $strOutputHTML = "";
                //Define paths for portability
                $strOutputHTML .= '<!--googleoff: all-->';
                $strOutputHTML .= '<div class="cookie_banner" style="' . $posicion . ': 0px;">';
                $strOutputHTML .= '<div class="container"><div class="row"><div class="col-xs-12 col-md-6"><p>' . JText::_($message) . '</p></div><div class="buttons col-xs-12 col-md-6"><a class="btn btn-success btn-cookie">' . JText::_($buttomText) . '</a> <a href="' . JText::_($URLLegal) . '" target="_blank" class="btn btn-primary">' . JText::_($urlLegalText) . '</a></div></div></div>';
                if ($agreeCookies == 'Yes') {
                    $strOutputHTML .= '<input type="hidden" class="agreeCookies" value="true" />';
                } else {
                    $strOutputHTML .= '<input type="hidden" class="agreeCookies" value="false" />';
                }
                if ($agreeAnalitycs == 'Yes') {
                    $strOutputHTML .= '<input type="hidden" class="agreeAnalitycs" value="true" />';
                } else {
                    $strOutputHTML .= '<input type="hidden" class="agreeAnalitycs" value="false" />';
                }
                $strOutputHTML .= '</div>';
                $strOutputHTML .= '<!--googleon: all-->';

                $body = JResponse::getBody();
                $body = str_replace('</body>', $strOutputHTML . '</body>', $body);
                JResponse::setBody($body);
            }
        }
    }

}
